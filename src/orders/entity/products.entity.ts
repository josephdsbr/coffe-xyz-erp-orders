import { ExternalBaseEntity } from './external-base.entity';

export class Products extends ExternalBaseEntity {
  imageUrl: string;
  name: string;
  price: number;
}
