import { Messages } from 'src/messages/messages';
import { IsNotEmpty, IsPositive } from 'class-validator';

export class ItemsDTO {
  @IsNotEmpty({ message: Messages.ITEM_PRODUCT_UID_CANNOT_BE_NULL })
  productUid: string;
  @IsNotEmpty({ message: Messages.ITEM_AMOUNT_CANNOT_BE_NULL })
  @IsPositive({ message: Messages.ITEM_AMOUNT_MUST_BE_POSITIVE })
  amount: number;
}
