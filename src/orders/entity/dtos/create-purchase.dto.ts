import { Type } from 'class-transformer';
import {
  ArrayMinSize,
  ArrayNotEmpty,
  IsArray,
  IsNotEmpty,
  IsNotEmptyObject,
  IsPositive,
  ValidateNested,
} from 'class-validator';
import { Messages } from 'src/messages/messages';
import { CardDTO } from './card.dto';
import { ItemsDTO } from './items.dto';

export class CreatePurchaseDTO {
  @ArrayNotEmpty({ message: Messages.PURCHASE_ITEMS_CANNOT_BE_EMPTY })
  @ArrayMinSize(1, {
    message: `${Messages.PURCHASE_ITEMS_MIN_SIZE_RESTRICTION}: 1`,
  })
  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => ItemsDTO)
  items: ItemsDTO[];

  @IsPositive({ message: Messages.PURCHASE_AMOUNT_MUST_BE_POSITIVE })
  @IsNotEmpty({ message: Messages.PURCHASE_AMOUNT_CANNOT_BE_EMPTY })
  amount: number;

  @ValidateNested()
  @IsNotEmptyObject(
    {
      nullable: false,
    },
    { message: Messages.CARD_OBJECT_CANNOT_BE_EMPTY_OR_HAVE_EMPTY_PROPERTIES },
  )
  card: CardDTO;
}
