export abstract class ExternalBaseEntity {
  id: number;
  uuid: string;
  createdAt: Date;
  updatedAt: Date;
  removedAt: Date;
}
