import { ExternalBaseEntity } from './external-base.entity';

export class Payments extends ExternalBaseEntity {
  amount: number;
  cardToken: string;
}
