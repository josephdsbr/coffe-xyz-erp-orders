import { DomainBaseEntity } from 'src/orders/entity/domain-base.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { Purchases } from './purchases.entity';

@Entity()
export class Items extends DomainBaseEntity<Items> {
  @ManyToOne(() => Purchases, (purchases: Purchases) => purchases.items)
  purchases: Purchases;
  // @OneToOne(() => Products)
  // products: Products;
  @Column()
  amount: number;

  setUpdatableFields(entity: Items): void {
    Object.assign(this, entity);
  }
}
