import { Items } from './items.entity';
import { DomainBaseEntity } from 'src/orders/entity/domain-base.entity';
import { Entity, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { Payments } from './payments.entity';

@Entity()
export class Purchases extends DomainBaseEntity<Purchases> {
  // @OneToOne(() => Payments)
  // @JoinColumn()
  // payments: Payments;

  @OneToMany(() => Items, (items: Items) => items.purchases)
  items: Items[];

  setUpdatableFields(entity: Purchases): void {
    Object.assign(this, entity);
  }
}
