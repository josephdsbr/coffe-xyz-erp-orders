import { PurchasesService } from './services/purchases.service';
import { CreatePurchaseDTO } from './entity/dtos/create-purchase.dto';
import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';

@Controller('orders')
export class OrdersController {
  constructor(private purchasesService: PurchasesService) {}

  @Post()
  async create(@Body(ValidationPipe) createPurchaseDTO: CreatePurchaseDTO) {
    return this.purchasesService.create(createPurchaseDTO);
  }

  // @Get('products-exists/:uid')
  // async productExists(@Param('uid') uid: string): Promise<boolean> {
  //   return this.itemService.exists(uid);
  // }
}
