import { ItemsDTO } from './../entity/dtos/items.dto';
import { Products } from './../entity/products.entity';
import { IGrpcService } from './../../models/grpc.interface';
import { microserviceOptions } from './../../grpc.option';
import { ItemsRepository } from './../repository/items.repository';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { catchError, map } from 'rxjs/operators';
import { Items } from '../entity/items.entity';
import { of } from 'rxjs';
import { Logger } from 'winston';
import { Inject } from '@nestjs/common';

@Injectable()
export class ItemsService implements OnModuleInit {
  @Client(microserviceOptions)
  private client: ClientGrpc;
  private grpcService: IGrpcService;

  constructor(
    private itemsRepository: ItemsRepository,
    @Inject('winston') private logger: Logger,
  ) {}

  onModuleInit() {
    this.grpcService = this.client.getService<IGrpcService>(
      'ProductsController',
    );
  }

  exists(uid: string): Promise<boolean> {
    return this.grpcService
      .productExists({ data: uid })
      .pipe(map((data) => data.exists))
      .toPromise();
  }

  async getItems(dto: ItemsDTO): Promise<Items> {
    const product = await this.grpcService
      .getByUid({ data: dto.productUid })
      .toPromise();
    const items = new Items();
    return items;
  }
}
