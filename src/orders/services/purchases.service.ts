import { ItemsService } from './items.service';
import { CreatePurchaseDTO } from './../entity/dtos/create-purchase.dto';
import { Injectable } from '@nestjs/common';
import { PurchasesRepository } from '../repository/purchases.repository';

@Injectable()
export class PurchasesService {
  constructor(
    private purchasesRepository: PurchasesRepository,
    private itemService: ItemsService,
  ) {}

  async create(dto: CreatePurchaseDTO) {
    const items = await Promise.all(
      dto.items.map((item) => this.itemService.getItems(item)),
    );
    return null;
  }
}
