import { PurchasesRepository } from './repository/purchases.repository';
import { ItemsRepository } from './repository/items.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { ItemsService } from './services/items.service';
import { PurchasesService } from './services/purchases.service';
import { OrdersController } from './orders.controller';
import { WinstonModule } from 'nest-winston';
import { winstonConfig } from 'src/config/winston.config';

@Module({
  imports: [
    TypeOrmModule.forFeature([ItemsRepository, PurchasesRepository]),
    WinstonModule.forRoot(winstonConfig),
  ],
  providers: [ItemsService, PurchasesService],
  controllers: [OrdersController],
})
export class OrdersModule {}
