import { Purchases } from './../entity/purchases.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Purchases)
export class PurchasesRepository extends Repository<Purchases> {}
