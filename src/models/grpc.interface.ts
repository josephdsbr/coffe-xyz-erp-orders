import { Observable } from 'rxjs';
import { Products } from 'src/orders/entity/products.entity';

export interface IGrpcService {
  productExists(data: UUID): Observable<GrpcReturn>;
  getByUid(data: UUID): Observable<Products>;
}

export interface UUID {
  data: string;
}

export interface GrpcReturn {
  exists: boolean;
}
