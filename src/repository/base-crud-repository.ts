import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { DomainBaseEntity } from '../orders/entity/domain-base.entity';

@Injectable()
export class BaseCrudRepository<
  T extends DomainBaseEntity<T>
> extends Repository<T> {
  async findAllByRemovedAtIsNull(): Promise<T[]> {
    return await this.find({ where: { removedAt: null } });
  }

  async findByUidAndRemovedAtIsNull(uid: string): Promise<T> {
    return await this.findOne(uid, { where: { removedAt: null } });
  }
}
