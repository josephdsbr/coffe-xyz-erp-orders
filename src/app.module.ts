import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';
import { typeOrmConfig } from './config/typeorm.config';
import { winstonConfig } from './config/winston.config';
import { OrdersModule } from './orders/orders.module';

@Module({
  imports: [
    OrdersModule,
    TypeOrmModule.forRoot(typeOrmConfig),
    WinstonModule.forRoot(winstonConfig),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
